 # Maintainer: Snogard <snogard at gmail dot com>

pkgname=bcml-pyenv
pkgver=3.8.6
pkgrel=1
pkgdesc="Breath of the Wild Cross-Platform Mod Loader: A mod merger and manager for BOTW"
arch=('x86_64')
url="https://github.com/NiceneNerd/BCML"
license=('GPL3')
groups=()
depends=()
makedepends=(
    'podman'
    'rsync'
)
provides=(bcml)
conflicts=(bcml-git)
options=(!strip)
source=(
    'bcml.desktop'
    'bcml.sh'
    'bcml-debug.sh'
    'bcml.png::https://camo.githubusercontent.com/4fef95be3957643b10d886fc2b551f3e71b51af5ce5af557ff212612dc187139/68747470733a2f2f692e696d6775722e636f6d2f4f69714b5078302e706e67'
)
sha256sums=('a9fe15655e8a09e0b906d3e712a1d8870661f77276f481a290486d41091acba1'
            '423ba2bfd71aa782446ee53e83a56bf253b651db224d6f1dac3053f3c3fdb977'
            '423ba2bfd71aa782446ee53e83a56bf253b651db224d6f1dac3053f3c3fdb977'
            'f20788bc2187b7b5d40d8b63a97b9cacdd986f8d0d77c6c456561e7fbd68179a')

_python_version=3.7.13

build() {
    containerName=bcml-pyenv-builder
    imageName=docker.io/lopsided/archlinux:devel

    cd $srcdir
    mkdir -p build

    podman pull $imageName
    podman run --rm -d \
        --name $containerName \
        -v $srcdir/build:/opt \
        $imageName \
        tail -f /dev/null

    podman exec $containerName bash -c \
        "pacman -Sy pyenv tar --needed --noconfirm \
        && export PYENV_ROOT='/opt/bcml-pyenv/pyenv' \
        && export HOME='/opt/bcml-pyenv/home' \
        && mkdir -p '/opt/bcml-pyenv/tmp' \
        && pyenv install $_python_version \
        && cd '/opt/bcml-pyenv/tmp' \
        && pyenv local $_python_version \
        && pyenv exec pip install pipx \
        && pyenv exec pipx install bcml==$pkgver \
        && pyenv exec pipx inject bcml 'pywebview[qt]'" \
        || podman stop $containerName

    podman stop $containerName
    rm -r build/bcml-pyenv/tmp build/bcml-pyenv/home/.cache build/bcml-pyenv/pyenv/shims
}

package() {
    install -Dm755 "$srcdir/bcml.desktop" "$pkgdir/usr/share/applications/bcml.desktop"
    install -Dm755 "$srcdir/bcml.png" "$pkgdir/usr/share/pixmaps/bcml.png"

    install -dm755 "$pkgdir/opt/bcml-pyenv"
    rsync -vaultp "$srcdir/build/bcml-pyenv" "$pkgdir/opt/"
    
    install -Dm755 "$srcdir/bcml.sh" "$pkgdir/usr/bin/bcml"
    install -Dm755 "$srcdir/bcml-debug.sh" "$pkgdir/usr/bin/bcml-debug"
}
